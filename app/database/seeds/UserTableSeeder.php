<?php
/**
 * User: dodywicaksono
 * Date: 2/16/14
 * Time: 5:21 PM
 */

class UserTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $faker = $this->getFaker();
        for ($i = 0; $i < 10; $i++) {
            $email    = $faker->email;
            $password = Hash::make("password");
            $id =User::create([
                "email"    => $email,
                "password" => $password
            ]);
        }
    }
}
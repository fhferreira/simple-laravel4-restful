<?php
/**
 * User: dodywicaksono
 * Date: 2/16/14
 * Time: 7:07 PM
 */

class ContactTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $faker = $this->getFaker();

        $users = User::all();

        foreach ($users as $k => $v) {

            for ($i = 0; $i < rand(-1, 15); $i++) {

                Contact::create([
                    "user_id"   => $v->id,
                    "firstName" => $faker->firstName,
                    "lastName"  => $faker->lastName,
                    "email"     => $faker->email,
                    "address"   => $faker->address,
                    "city"      => $faker->city,
                    "postCode"  => $faker->postcode,
                    "province"  => $faker->city,
                    "country"   => $faker->countryCode,
                    "phone"     => $faker->phoneNumber,
                    "fax"       => $faker->phoneNumber
                ]);

            }

        }

    }
}